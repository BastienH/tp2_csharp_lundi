﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp2
{
    class Program
    {
        public static int SommeEntier(int min, int max)
        {
            int s = 0;

            for (int i = min; i <= max; ++i)
            {
                s += i;
            }
            return s;
        }

        public static double moy(List<double> d)
        {
            double m = 0.0;

            foreach(double i in d)
            {
                m += i;
            }

            return m / d.Count();
        }

        public static int tp()
        {
            List<int> l1 = new List<int>();
            List<int> l2 = new List<int>();
            int i = 1, s = 0;

            while (3 * i <= 100)
            {
                l1.Add(3 * i);
                ++i;
            }

            i = 1;

            while (5 * i <= 100)
            {
                l2.Add(5 * i);
                ++i;
            }

            foreach(int j in l1)
            {
            
                if(l2.Contains(j))
                {
                    s += j;
                }
                
            }

            

            return s;

        }

        public static void Game()
        {
            bool trouve = false;
            var rand = new Random();
            int alea = rand.Next(1, 100);
            int guess = -1;
            bool ok = false;

            //Console.WriteLine(alea.Next(1, 100));

            do
            {
                ok = false;
                guess = -1;
                do
                {
                    try
                    {
                        Console.WriteLine("Donner un entier : ");
                        guess = Convert.ToInt16(Console.ReadLine());
                        ok = true;
                    }
                    catch (System.FormatException) { };

                } while (!ok);

                if (guess < alea)
                {
                    Console.WriteLine("c++");
                }
                else if (guess > alea)
                {
                    Console.WriteLine("c--");
                }
                else
                {
                    trouve = true;
                }
            } while (!trouve);

            Console.WriteLine("gg !");
            Console.Read();
        }
        static void Main(string[] args)
        {
            /*int h = DateTime.Now.Hour;
            String nom = Environment.UserName;
            DayOfWeek d = DateTime.Now.DayOfWeek;
            
            switch(d)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    if (h >= 9 && h < 18)
                    {
                        Console.WriteLine("Bonjour " + nom);
                    }
                    else
                    {
                        Console.WriteLine("Bonsoir " + nom);
                    }
                    break;
                case DayOfWeek.Friday:
                    if (h >= 9 && h < 18)
                    {
                        Console.WriteLine("Bonjour " + nom);
                    }
                    else
                    {
                        Console.WriteLine("Bon week end" + nom);
                    }
                    break;
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    
                    Console.WriteLine("Bon week end " + nom);
                    break;

            }*/

            /*List<double> d = new List<double>();

            for(int i = 1; i <= 10; ++i)
            {
                d.Add(i);
            }

            Console.WriteLine(SommeEntier(1, 10));
            Console.WriteLine(moy(d));
            Console.WriteLine(tp());
            Console.Read();*/

            Game();
        }
    }
}
